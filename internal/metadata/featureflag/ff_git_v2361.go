package featureflag

// GitV2361Gl1 will enable use of Git v2.36.1.gl1.
var GitV2361Gl1 = NewFeatureFlag("git_v2361gl1", false)
