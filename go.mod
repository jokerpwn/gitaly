module gitlab.com/gitlab-org/gitaly/v15

require (
	github.com/avast/retry-go v3.0.0+incompatible // indirect
	github.com/beevik/ntp v0.3.0
	github.com/cloudflare/tableflip v1.2.2
	github.com/containerd/cgroups v0.0.0-20201118023556-2819c83ced99
	github.com/dpotapov/go-spnego v0.0.0-20220426193508-b7f82e4507db // indirect
	github.com/getsentry/sentry-go v0.13.0
	github.com/git-lfs/git-lfs/v3 v3.2.0
	github.com/go-enry/go-license-detector/v4 v4.3.0
	github.com/go-git/go-git/v5 v5.3.0 // indirect
	github.com/google/go-cmp v0.5.8
	github.com/google/uuid v1.3.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/hashicorp/go-uuid v1.0.3 // indirect
	github.com/hashicorp/golang-lru v0.5.4
	github.com/hashicorp/yamux v0.0.0-20210316155119-a95892c5f864
	github.com/jackc/pgconn v1.10.1
	github.com/jackc/pgtype v1.9.1
	github.com/jackc/pgx/v4 v4.14.1
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/libgit2/git2go/v33 v33.0.9
	github.com/olekukonko/tablewriter v0.0.5
	github.com/opencontainers/runtime-spec v1.0.2
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pelletier/go-toml v1.9.5
	github.com/prometheus/client_golang v1.12.1
	github.com/prometheus/client_model v0.2.0
	github.com/rubenv/sql-migrate v0.0.0-20191213152630-06338513c237
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.2
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	gitlab.com/gitlab-org/gitlab-shell v1.9.8-0.20210720163109-50da611814d2
	gitlab.com/gitlab-org/labkit v1.14.0
	go.uber.org/goleak v1.1.12
	gocloud.dev v0.23.0
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5 // indirect
	golang.org/x/net v0.0.0-20220531201128-c960675eff93 // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f
	golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c
	golang.org/x/time v0.0.0-20220609170525-579cf78fd858
	google.golang.org/grpc v1.40.1
	google.golang.org/protobuf v1.28.0
)

go 1.16
